import 'dart:async';

import 'package:flutter/material.dart';

class TimerScreen extends StatefulWidget {
  const TimerScreen({super.key});

  @override
  _TimerScreenState createState() => _TimerScreenState();
}

class _TimerScreenState extends State<TimerScreen> {
  Timer? _stopwatchTimer;
  Timer? _countdownTimer;
  bool _isStopwatchRunning = false;
  bool _isCountdownRunning = false;
  int _stopwatchSeconds = 0;
  int _countdownSeconds = 0;
  int _countdownDuration = 0; // Duration in seconds

  @override
  void dispose() {
    _stopwatchTimer?.cancel();
    _countdownTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Timer Demo'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              _formatTime(_stopwatchSeconds),
              style: const TextStyle(fontSize: 36),
            ),
            const SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: _toggleStopwatch,
                  child: Text(_isStopwatchRunning ? 'Stop' : 'Start'),
                ),
                const SizedBox(width: 20),
                ElevatedButton(
                  onPressed: _resetStopwatch,
                  child: const Text('Reset'),
                ),
              ],
            ),
            const SizedBox(height: 40),
            Text(
              _formatTime(_countdownSeconds),
              style: const TextStyle(fontSize: 36),
            ),
            const SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: _toggleCountdown,
                  child: Text(_isCountdownRunning ? 'Stop' : 'Start'),
                ),
                const SizedBox(width: 20),
                ElevatedButton(
                  onPressed: _resetCountdown,
                  child: const Text('Reset'),
                ),
              ],
            ),
            const SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text('Set Countdown: '),
                const SizedBox(width: 10),
                _buildTimeInput(),
                const SizedBox(width: 10),
                ElevatedButton(
                  onPressed: _setCountdown,
                  child: const Text('Set'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _toggleStopwatch() {
    setState(() {
      _isStopwatchRunning = !_isStopwatchRunning;
      if (_isStopwatchRunning) {
        _startStopwatch();
      } else {
        _stopStopwatch();
      }
    });
  }

  void _startStopwatch() {
    _stopwatchTimer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        _stopwatchSeconds++;
      });
    });
  }

  void _stopStopwatch() {
    _stopwatchTimer?.cancel();
  }

  void _resetStopwatch() {
    setState(() {
      _stopwatchSeconds = 0;
    });
  }

  void _toggleCountdown() {
    setState(() {
      _isCountdownRunning = !_isCountdownRunning;
      if (_isCountdownRunning) {
        _startCountdown();
      } else {
        _stopCountdown();
      }
    });
  }

  void _startCountdown() {
    _countdownTimer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        if (_countdownSeconds > 0) {
          _countdownSeconds--;
        } else {
          _stopCountdown();
          _showTimeUpPopup();
        }
      });
    });
  }

  void _stopCountdown() {
    _countdownTimer?.cancel();
  }

  void _resetCountdown() {
    setState(() {
      _countdownSeconds = 0;
      _isCountdownRunning = false;
    });
  }

  void _setCountdown() {
    setState(() {
      _countdownSeconds = _countdownDuration;
    });
  }

  void _showTimeUpPopup() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Time is Up'),
          content: const Text('The time is over!!!'),
          actions: [
            ElevatedButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildTimeInput() {
    return SizedBox(
      width: 50,
      child: TextField(
        keyboardType: TextInputType.number,
        textAlign: TextAlign.center,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
        ),
        onChanged: (value) {
          setState(() {
            _countdownDuration = int.tryParse(value) ?? 0;
          });
        },
      ),
    );
  }

  String _formatTime(int seconds) {
    int hours = seconds ~/ 3600;
    int minutes = (seconds % 3600) ~/ 60;
    int remainingSeconds = seconds % 60;
    return '${_formatTwoDigits(hours)}:${_formatTwoDigits(minutes)}:${_formatTwoDigits(remainingSeconds)}';
  }

  String _formatTwoDigits(int value) {
    return value.toString().padLeft(2, '0');
  }
}