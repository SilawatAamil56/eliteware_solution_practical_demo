import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  bool _isLoading = false;
  bool _isPasswordVisible = false;
  String? _error;

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }


  showToast(String msg){
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(msg),
      ),
    );
  }

  Future<void> _login() async {
    setState(() {
      _isLoading = true;
      _error = null;
    });

    final username = _usernameController.text;
    final password = _passwordController.text;

    // Replace with your login API endpoint
    const apiUrl = 'http://restapi.adequateshop.com/api/authaccount/login';

    try {
      HttpClient httpClient = HttpClient()
        ..badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

      final dio = Dio()
      ..httpClientAdapter = DefaultHttpClientAdapter();

    dio.interceptors.add(InterceptorsWrapper(
        onRequest: (options, handler) { options.headers['Content-Type'] = 'application/json';
          return handler.next(options);
        },
      ));



      final response = await dio.post(apiUrl, data: {
        'email': username,
        'password': password,
      });

      if (response.data["code"] == 0) {
        // Successful login
        final data = response.data;
        final token = data['token'];
        // Do something with the token
        if (kDebugMode) {
          print('Login successful. Token: $token');
        }
        showToast(data['message']);



      } else {
        // Login failed
        setState(() {
          _error = 'Invalid username or password';
          showToast("Invalid username or password");
        });
      }
    } catch (error) {
      // Error occurred during login
      if (kDebugMode) {
        print('Error: $error');
      }
      setState(() {
        _error = 'An error occurred. Please try again later.';
      });
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login Demo'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Use Email as an  Developer5@gmail.com and 123456 as password"),
              SizedBox(height: 25,),
              TextFormField(
                controller: _usernameController,
                decoration: const InputDecoration(
                  labelText: 'Email',
                  prefixIcon: Icon(Icons.email),
                ),
                keyboardType: TextInputType.emailAddress,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your email';
                  }
                  if (!value.contains('@') || !value.contains('.')) {
                    return 'Please enter a valid email address';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: _passwordController,
                obscureText: !_isPasswordVisible,
                decoration: InputDecoration(
                  labelText: 'Password',
                  prefixIcon: Icon(Icons.lock),
                  suffixIcon: IconButton(
                    icon: Icon(
                      _isPasswordVisible
                          ? Icons.visibility
                          : Icons.visibility_off,
                    ),
                    onPressed: () {
                      setState(() {
                        _isPasswordVisible = !_isPasswordVisible;
                      });
                    },
                  ),
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter your password';
                  }
                  // Add any additional password validation logic here
                  return null;
                },
              ),
              const SizedBox(height: 20),
              if (_error != null)
                Text(
                  _error!,
                  style: const TextStyle(color: Colors.red),
                ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: _isLoading ? null : _submitForm,
                child: _isLoading
                    ? const CircularProgressIndicator()
                    : const Text('Login'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _submitForm() {
    final isValid = _formKey.currentState!.validate();

    if (isValid) {
      _login();
    }
  }
}
